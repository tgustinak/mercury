#!/usr/bin/env bash

# git
echo ""
echo ""
echo "## -> Installing git"
sudo apt-get install git

# vim
echo ""
echo ""
echo "## -> Installing VIM"
sudo apt-get install vim

# composer
echo ""
echo ""
echo "## -> Installing Composer"
sudo apt-get -y install composer

# nodejs
echo ""
echo ""
echo "## -> Installing NodeJS"
sudo apt-get -y install nodejs

# supervisor
echo ""
echo ""
echo "## -> Installing Supervisor"
sudo apt-get -y install supervisor